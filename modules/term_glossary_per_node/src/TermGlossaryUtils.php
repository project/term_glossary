<?php

namespace Drupal\term_glossary_per_node;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;

/**
 * Various utilities methods for term_glossary_per_node.
 */
class TermGlossaryUtils {

  /**
   * Get the glossary display state of a node.
   */
  public static function isNodeGlossaryActive(
    NodeInterface $node,
    $term_glossary_active_default = TRUE,
  ): bool {
    // If no value has been set on the node, use the default value.
    if ($node->hasField('term_glossary_active')) {
      $active_field = $node->get('term_glossary_active');
      if ($node->isNew() || $active_field->isEmpty()) {
        // The node is new, or the field is empty, use default value.
        $term_glossary_active = $term_glossary_active_default;
      }
      else {
        $term_glossary_active = $active_field->value;
      }
    }
    else {
      // Field doesn't exist, use default value.
      $term_glossary_active = $term_glossary_active_default;
    }
    return $term_glossary_active;
  }

  /**
   * Check if user is authorized to update glossary state.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The user to check permissions for.
   *
   * @return bool
   *   TRUE if the user can change glossary display, FALSE otherwise.
   */
  public static function hasEditGlossaryPermission(AccountProxyInterface $currentUser): bool {
    return $currentUser->hasPermission('edit term glossary node display') ||
      $currentUser->hasPermission('administer nodes');
  }

}
