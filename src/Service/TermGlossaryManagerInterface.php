<?php

namespace Drupal\term_glossary\Service;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterInterface;

/**
 * The TermGlossary manager service interface.
 */
interface TermGlossaryManagerInterface {

  /**
   * Inject terms in the field value.
   *
   * @param string $input
   *   Original field string.
   * @param array|string $vocabularies
   *   The names of the vocabularies to use.
   * @param null|\Drupal\Core\Entity\EntityInterface $root_entity
   *   The root content of the field being processed.
   *
   * @return false|string
   *   String with highlighted terms or false if not match found.
   */
  public function replaceFieldValue($input, $vocabularies, $root_entity = NULL);

  /**
   * Adds required libraries and settings to the template variables array.
   *
   * @param array $variables
   *   The template variables being passed to the preprocess hook.
   */
  public function attachLibrariesAndSettings(&$variables);

  /**
   * Get configuration.
   *
   * @param string $key
   *   Config key.
   *
   * @return string|null
   *   Config value.
   */
  public function getConfigValue(string $key): ?string;

  /**
   * Get vocabularies from preprocess variables array.
   */
  public function getVocabulariesFromFieldPreprocessVariables(&$variables);

  /**
   * Gets the field formatter term glossary third party settings elements.
   */
  public function getFieldFormatterThirdPartySettingsElements(FormatterInterface $plugin, FieldDefinitionInterface $field_definition);

  /**
   * Gets the parent root entity from the entity we are processing.
   */
  public function getRootEntityFromFieldPreprocessVariables($variables);

  /**
   * Flush the glossary terms cache.
   */
  public function invalidateTermsCache($vocab);

  /**
   * Gets the configured term glossary handler.
   */
  public function getHandler();

}
