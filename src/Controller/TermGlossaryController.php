<?php

namespace Drupal\term_glossary\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * The TermGlossary controller.
 */
class TermGlossaryController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The term_glossary module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $glossaryConfig;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->configManager = $container->get('config.manager');
    $instance->requestStack = $container->get('request_stack');
    $instance->entityRepository = $container->get('entity.repository');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Api Search Per Letter.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   */
  public function apiSearchPerLetter($letter) {
    $results = ['message' => $this->t('No results found for @letter', ['@letter' => $letter])];
    $status = 200;
    if (!empty($letter) && preg_match("/^[a-zA-Z]$/", strtoupper($letter))) {
      $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
      $vocabularies = $this->getVocabularies();
      $query->condition('name', $letter, 'STARTS_WITH');
      $query->condition('vid', $vocabularies, 'IN');
      $query->condition('langcode', $this->languageManager()->getCurrentLanguage()->getId());
      $query->accessCheck();
      $entity_ids = $query->execute();
      if (count($entity_ids) !== 0) {
        $results = [];
        $view_mode = $this->getConfig('view_mode');
        $view_builder = $this->entityTypeManager->getViewBuilder('taxonomy_term');
        $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
        $terms = $term_storage->loadMultiple($entity_ids);
        foreach ($terms as $term) {
          $term = $this->entityRepository->getTranslationFromContext($term);
          $results[] = $this->buildTermResult($term, $view_mode, $view_builder);
        }
        $this->moduleHandler->invokeAll('term_glossary_alter_results', [
          &$results,
          $terms,
          $letter,
        ]);
      }
    }
    return new JsonResponse($results, $status);
  }

  /**
   * Helper to get term by id.
   */
  public function apiGetTermById($tid) {
    $results = ['message' => $this->t('No results found for @tid', ['@tid' => $tid])];
    $status = 200;
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $term = $term_storage->load($tid);
    if (!empty($term)) {
      $term = $this->entityRepository->getTranslationFromContext($term);
      $view_mode = $this->getConfig('view_mode');
      $view_builder = $this->entityTypeManager->getViewBuilder('taxonomy_term');
      $results = $this->buildTermResult($term, $view_mode, $view_builder);
    }
    $this->moduleHandler->invokeAll('term_glossary_alter_result', [
      &$results,
      $term,
      $tid,
    ]);

    return $this->buildCachedJsonResponse($results, $status);
  }

  /**
   * Helper to search per term.
   */
  public function apiSearchPerTerm() {
    $request = $this->requestStack->getCurrentRequest();
    // 200 because js is not checking for error.
    $status = 200;
    $results = ['message' => $this->t('error bad request')];
    if (!empty($request->get('t'))) {
      // @todo make sure this is "safe" here.
      $term = Html::escape(trim($request->get('t')));
      if (!empty($term)) {
        $results = [];
        $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
        $query = $term_storage->getQuery();
        $vocabularies = $this->getVocabularies();
        $query->condition('name', $term, 'CONTAINS');
        $query->condition('vid', $vocabularies, 'IN');
        $query->condition('langcode', $this->languageManager()->getCurrentLanguage()->getId());
        $query->accessCheck();
        $entity_ids = $query->execute();

        if (count($entity_ids) == 0) {
          $query = $term_storage->getQuery();
          $query->condition('name', "%" . $this->database->escapeLike($term) . "%", 'LIKE');
          $query->condition('vid', $vocabularies, 'IN');
          $query->condition('langcode', $this->languageManager()->getCurrentLanguage()->getId());
          $query->accessCheck();
          $entity_ids = $query->execute();
        }
        if (count($entity_ids) == 0) {
          $results = ['message' => $this->t('No results found for search term "@term"', ['@term' => $term])];
        }
        else {
          $view_mode = $this->getConfig('view_mode');
          $view_builder = $this->entityTypeManager->getViewBuilder('taxonomy_term');
          $terms = $term_storage->loadMultiple($entity_ids);
          foreach ($terms as $term) {
            $term = $this->entityRepository->getTranslationFromContext($term);
            $results[$term->id()] = $this->buildTermResult($term, $view_mode, $view_builder);
            // Perhaps fire hook to include more.
          }
          $this->moduleHandler->invokeAll('term_glossary_alter_results', [
            &$results,
            $terms,
            $term,
          ]);
        }

      }
    }
    return new JsonResponse($results, $status);
  }

  /**
   * Function to get the vocab name form config.
   *
   * @return string[]
   *   the vocab name from config.
   */
  private function getVocabularies() {
    $vocabularies_list = $this->getConfig('vocab');
    if (empty($vocabularies_list)) {
      throw new \Exception('glossary has not been configured yet');
    }
    return explode(',', $vocabularies_list);
  }

  /**
   * Gets the term_glossary module configuration.
   */
  protected function getGlossaryConfig() {
    if (!isset($this->glossaryConfig)) {
      $config_factory = $this->configManager->getConfigFactory();
      $this->glossaryConfig = $config_factory->get('term_glossary.glossaryconfig');
    }
    return $this->glossaryConfig;
  }

  /**
   * Gets a configuration item from the term_glossary config module.
   */
  protected function getConfig(string $name) {
    return $this->getGlossaryConfig()->get($name);
  }

  /**
   * Builds a term data array for the JSON response.
   */
  protected function buildTermResult(EntityInterface $term, string $view_mode, EntityViewBuilderInterface $view_builder) {
    $result = [
      'tid' => $term->id(),
      'lang' => $term->language()->getId(),
      'name' => $term->getName(),
      'description' => $term->getDescription(),
    ];
    if (empty($view_mode)) {
      $html_output = "<p><strong>" . Html::escape($term->getName()) . "</strong></p>";
      $html_output .= Xss::filter($term->getDescription());
    }
    else {
      $build = $view_builder->view($term, $view_mode);
      $html_output = $this->renderer->render($build);
    }
    $result['html'] = $html_output;
    return $result;
  }

  /**
   * Adds default cache headers to the JSON response.
   */
  protected function buildCachedJsonResponse(mixed $results, int $status) {
    $response = new JsonResponse($results, $status);
    $max_age = $this->getConfig('json_term_cache');
    if (!is_numeric($max_age)) {
      // Default to 1-hour cache if not defined or empty.
      $max_age = 1 * 60 * 60;
    }
    if ($max_age > 0) {
      $response->setPublic();
      $response->setMaxAge($max_age);
      $response->setSharedMaxAge($max_age);
      $expires = new \DateTime();
      $expires->setTimestamp(time() + $max_age);
      $response->setExpires($expires);
    }
    return $response;
  }

}
