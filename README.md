# Taxonomy Term Glossary

This flexible module uses a vocabulary (taxonomy) as a glossary.
it auto scans the fields (you configure it to) for uses of taxonomy
terms defined in your vocabulary.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/term_glossary).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/term_glossary).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

**Enable Taxonomy term glossary**

To enable the module, go to `/admin/config/glossary` and choose the
taxonomy vocabulary to use as source of highlighted terms.

**Enable Term glossary filter**

Taxonomy term glossary will parse the entity text fields rendered
string to add html tags around found words.
Go to the display options of the text fields you want to parse in the entity
display forms and check the option "Enable term glossary"

### Per-term matching options (Experimental)

To use per-term matching options, you need to add some of the following
numerical fields on your taxonomy:

- `field_full_word` to allow overriding the global full-word matching
configuration.
- `field_case_sensitive` to allow overriding the global case-sensitivity
matching configuration.

If the taxonomy term field contains a `0` value, it will disable the global
configuration for this term.

If the taxonomy term field contains a `1` value, it will enforce the term
matching option over the global one.

Every other value will have no effect, the global matching options will be used.

You finally need to enable the "Per term matching options" configuration option
on the module settings page.

### Term synonyms (Experimental)

To enable the use of term synonyms, you will need to add a multivalued string
field to your taxonomy vocabulary and select it on the module configuration page
after enabling the "Use term synonyms" configuration option.

### Plugins (Experimental)

The Taxonomy Term Glossary module can now be extended using plugins.

An example submodule has been added to the project to help others create new
plugins. It is located in the `modules/term_glossary_abbr` directory.

It implements a simple handler plugin that creates `ABBR` elements instead of
`SPAN` for matched terms and synonyms in content text.

#### Tippy.js plugin

The **[Tippy.js](https://atomiks.github.io/tippyjs/)** plugin allows to display
your terms descriptions in nice looking tooltips.

You first need to have `popper.js` and `tippy.js` libraries available in your
`/web/libraries` folder of your Drupal installation.

- **popper.js** can be downloaded [here](https://asset-packagist.org/package/npm-asset/popperjs--core).
- **tippy.js** can be downloaded [here](https://asset-packagist.org/package/detail?fullname=npm-asset/tippy.js).

The plugin needs to access the following files, so check the paths are correct:

- `/libraries/popperjs/dist/umd/popper.js`
- `/libraries/tippyjs/dist/tippy.umd.js`
- `/libraries/tippyjs/dist/tippy.css`

You then have to install the submodule **Taxonomy Term Glossary Tippy.js
Tooltips** containing the plugin we need to display Tippy.js tooltips.

Once installed, go to the Taxonomy Term Glossary module configuration page
(`/admin/config/glossary`) and choose **Tippy.js** in the integration type
select field.

You can then choose the type of HTML tag (`span` or `abbr`) to use to render the
matched terms and which tag attribute to use to store the term description that
will be displayed in the tooltips by **Tippy.js**.

And you should be ready to go.

## Maintainers

- Bálint Nagy - [nagy.balint](https://www.drupal.org/u/nagybalint)
- olivier.br - [olivier.br](https://www.drupal.org/u/olivierbr)
- Taggart Jensen - [taggartj](https://www.drupal.org/u/taggartj)
- Frank Mably - [mably](https://www.drupal.org/u/mably)
